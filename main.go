package main

import (
	"encoding/json"
	"fmt"
	"log"

	"parser/lib/parser"
)

func main() {
	program := `
function sum(x, iter) {
	let result = 0;
	for (let i=0; i<iter; i+=1) {
		result = result + x;
	}
	return result;
}
sum(10, 10);
	`

	pr := parser.NewParser(program)

	ast, err := pr.Parse()
	if err != nil {
		log.Fatal("Parse error", err)
	}

	j, err := json.MarshalIndent(ast, "", "    ")
	if err != nil {
		log.Fatal("JSON-encode error", err)
	}

	fmt.Println(string(j))
}
