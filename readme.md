# Parser

A very simple parser for a very simple JavaScript-like syntax.

Given a program, it prints out an AST in json format.

## Note

This is hackathon-style code:
* The tests are not comprehensive.
* Error handling is not done properly.
* Code organization can be done better.
* The AST is using `map[string]any` not a proper struct type.
* There are many limitations.

## Limitations

* There are only string, int64 number, and boolean literals.
* Variables are untyped.
* All numbers are int64, no float.
* No array, no associative-array, no map, no dictionary.
* No OOP, no classes, etc.

## Run Tests

`go test -v -race ./...`

## Run

Type the program to be parsed in main.go inside program := `...` and then:

`go run .`
