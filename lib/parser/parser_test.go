package parser_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"parser/lib/parser"
)

func TestParse(t *testing.T) {
	t.Parallel()

	tt := []struct {
		name    string
		program string
		ast     map[string]any
	}{
		{
			name:    "Number",
			program: "27",
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type":       parser.ExpressionStatement,
						"expression": map[string]any{"type": parser.NumericLiteral, "value": int64(27)},
					},
				},
			},
		},
		{
			name:    "Single Quote String",
			program: "'xyzabc'",
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type":       parser.ExpressionStatement,
						"expression": map[string]any{"type": parser.StringLiteral, "value": "xyzabc"},
					},
				},
			},
		},
		{
			name:    "Double Quote String",
			program: `"xyzabc"`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type":       parser.ExpressionStatement,
						"expression": map[string]any{"type": parser.StringLiteral, "value": "xyzabc"},
					},
				},
			},
		},
		{
			name:    "Space",
			program: "  \n   ",
			ast: map[string]any{
				"type": parser.Program,
				"body": []map[string]any{nil},
			},
		},
		{
			name:    "Single-line Comment",
			program: "// comment\n27",
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type":       parser.ExpressionStatement,
						"expression": map[string]any{"type": parser.NumericLiteral, "value": int64(27)},
					},
				},
			},
		},
		{
			name:    "Multi-line Comment",
			program: "/* this is a comment\nthis is still comment */\n27",
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type":       parser.ExpressionStatement,
						"expression": map[string]any{"type": parser.NumericLiteral, "value": int64(27)},
					},
				},
			},
		},
		{
			name: "Multiple Expressions",
			program: `
				"xyzabc";
				27;
			`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type":       parser.ExpressionStatement,
						"expression": map[string]any{"type": parser.StringLiteral, "value": "xyzabc"},
					},
					{
						"type":       parser.ExpressionStatement,
						"expression": map[string]any{"type": parser.NumericLiteral, "value": int64(27)},
					},
				},
			},
		},
		{
			name: "Block Expressions",
			program: `
				{
					"xyzabc";
					27;
				}
			`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.BlockStatement,
						"body": []map[string]any{
							{
								"type":       parser.ExpressionStatement,
								"expression": map[string]any{"type": parser.StringLiteral, "value": "xyzabc"},
							},
							{
								"type":       parser.ExpressionStatement,
								"expression": map[string]any{"type": parser.NumericLiteral, "value": int64(27)},
							},
						},
					},
				},
			},
		},
		{
			name: "Empty Block",
			program: `
				{
				}
			`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.BlockStatement,
						"body": []map[string]any(nil),
					},
				},
			},
		},
		{
			name: "Nested Block",
			program: `
				{
					"xyzabc";
					{
						27;
					}
				}
			`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.BlockStatement,
						"body": []map[string]any{
							{
								"type":       parser.ExpressionStatement,
								"expression": map[string]any{"type": parser.StringLiteral, "value": "xyzabc"},
							},
							{
								"type": parser.BlockStatement,
								"body": []map[string]any{
									{
										"type":       parser.ExpressionStatement,
										"expression": map[string]any{"type": parser.NumericLiteral, "value": int64(27)},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name:    "Empty Statement",
			program: `;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.EmptyStatement,
					},
				},
			},
		},
		{
			name:    "Additive Expression",
			program: `2 - 3;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type":     parser.BinaryExpression,
							"operator": "-",
							"left": map[string]any{
								"type":  parser.NumericLiteral,
								"value": int64(2),
							},
							"right": map[string]any{
								"type":  parser.NumericLiteral,
								"value": int64(3),
							},
						},
					},
				},
			},
		},
		{
			name:    "Multiple Additive Expression",
			program: `1 + 2 - 3;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type":     parser.BinaryExpression,
							"operator": "-",
							"left": map[string]any{
								"type":     parser.BinaryExpression,
								"operator": "+",
								"left": map[string]any{
									"type":  parser.NumericLiteral,
									"value": int64(1),
								},
								"right": map[string]any{
									"type":  parser.NumericLiteral,
									"value": int64(2),
								},
							},
							"right": map[string]any{
								"type":  parser.NumericLiteral,
								"value": int64(3),
							},
						},
					},
				},
			},
		},
		{
			name:    "Multiplicative Expression",
			program: `2 * 3;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type":     parser.BinaryExpression,
							"operator": "*",
							"left": map[string]any{
								"type":  parser.NumericLiteral,
								"value": int64(2),
							},
							"right": map[string]any{
								"type":  parser.NumericLiteral,
								"value": int64(3),
							},
						},
					},
				},
			},
		},
		{
			name:    "Multiple Expression",
			program: `1 + 2 * 3;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type":     parser.BinaryExpression,
							"operator": "+",
							"left": map[string]any{
								"type":  parser.NumericLiteral,
								"value": int64(1),
							},
							"right": map[string]any{
								"type":     parser.BinaryExpression,
								"operator": "*",
								"left": map[string]any{
									"type":  parser.NumericLiteral,
									"value": int64(2),
								},
								"right": map[string]any{
									"type":  parser.NumericLiteral,
									"value": int64(3),
								},
							},
						},
					},
				},
			},
		},
		{
			name:    "Parenthesized Expression",
			program: `(1 + 2) * 3;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type":     parser.BinaryExpression,
							"operator": "*",
							"left": map[string]any{
								"type":     parser.BinaryExpression,
								"operator": "+",
								"left": map[string]any{
									"type":  parser.NumericLiteral,
									"value": int64(1),
								},
								"right": map[string]any{
									"type":  parser.NumericLiteral,
									"value": int64(2),
								},
							},
							"right": map[string]any{
								"type":  parser.NumericLiteral,
								"value": int64(3),
							},
						},
					},
				},
			},
		},
		{
			name:    "Simple Assignment",
			program: `x = 27;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type":     parser.AssignmentExpression,
							"operator": "=",
							"left": map[string]any{
								"type": parser.Identifier,
								"name": "x",
							},
							"right": map[string]any{
								"type":  parser.NumericLiteral,
								"value": int64(27),
							},
						},
					},
				},
			},
		},
		{
			name:    "Complex Assignment",
			program: `a = b = c + 1;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type":     parser.AssignmentExpression,
							"operator": "=",
							"left": map[string]any{
								"type": parser.Identifier,
								"name": "a",
							},
							"right": map[string]any{
								"type":     parser.AssignmentExpression,
								"operator": "=",
								"left": map[string]any{
									"type": parser.Identifier,
									"name": "b",
								},
								"right": map[string]any{
									"type":     parser.BinaryExpression,
									"operator": "+",
									"left": map[string]any{
										"type": parser.Identifier,
										"name": "c",
									},
									"right": map[string]any{
										"type":  parser.NumericLiteral,
										"value": int64(1),
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name:    "Variable Declaration and Assignment",
			program: `let x = 27;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.VariableDeclarationStatement,
						"declarations": []map[string]any{
							{
								"type": parser.VariableDeclarator,
								"id": map[string]any{
									"type": parser.Identifier,
									"name": "x",
								},
								"init": map[string]any{
									"type":  parser.NumericLiteral,
									"value": int64(27),
								},
							},
						},
					},
				},
			},
		},
		{
			name:    "Multiple Declaration",
			program: `let x, y;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.VariableDeclarationStatement,
						"declarations": []map[string]any{
							{
								"type": parser.VariableDeclarator,
								"id": map[string]any{
									"type": parser.Identifier,
									"name": "x",
								},
								"init": map[string]any(nil),
							},
							{
								"type": parser.VariableDeclarator,
								"id": map[string]any{
									"type": parser.Identifier,
									"name": "y",
								},
								"init": map[string]any(nil),
							},
						},
					},
				},
			},
		},
		{
			name: "If-Else Statement",
			program: `
				if (foo) {
					foo = 27;
				} else {
					foo = 72;
				}
			`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.IfStatement,
						"test": map[string]any{
							"type": parser.Identifier,
							"name": "foo",
						},
						"consequent": map[string]any{
							"type": parser.BlockStatement,
							"body": []map[string]any{
								{
									"type": parser.ExpressionStatement,
									"expression": map[string]any{
										"type":     parser.AssignmentExpression,
										"operator": "=",
										"left": map[string]any{
											"type": parser.Identifier,
											"name": "foo",
										},
										"right": map[string]any{
											"type":  parser.NumericLiteral,
											"value": int64(27),
										},
									},
								},
							},
						},
						"alternate": map[string]any{
							"type": parser.BlockStatement,
							"body": []map[string]any{
								{
									"type": parser.ExpressionStatement,
									"expression": map[string]any{
										"type":     parser.AssignmentExpression,
										"operator": "=",
										"left": map[string]any{
											"type": parser.Identifier,
											"name": "foo",
										},
										"right": map[string]any{
											"type":  parser.NumericLiteral,
											"value": int64(72),
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "If Statement",
			program: `
				if (foo) {
					foo = 27;
				}
			`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.IfStatement,
						"test": map[string]any{
							"type": parser.Identifier,
							"name": "foo",
						},
						"consequent": map[string]any{
							"type": parser.BlockStatement,
							"body": []map[string]any{
								{
									"type": parser.ExpressionStatement,
									"expression": map[string]any{
										"type":     parser.AssignmentExpression,
										"operator": "=",
										"left": map[string]any{
											"type": parser.Identifier,
											"name": "foo",
										},
										"right": map[string]any{
											"type":  parser.NumericLiteral,
											"value": int64(27),
										},
									},
								},
							},
						},
						"alternate": map[string]any(nil),
					},
				},
			},
		},
		{
			name:    "Comparison",
			program: `x > 1;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type":     parser.BinaryExpression,
							"operator": ">",
							"left": map[string]any{
								"type": parser.Identifier,
								"name": "x",
							},
							"right": map[string]any{
								"type":  parser.NumericLiteral,
								"value": int64(1),
							},
						},
					},
				},
			},
		},
		{
			name:    "Equality",
			program: `false == null;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type":     parser.BinaryExpression,
							"operator": "==",
							"left": map[string]any{
								"type":  parser.BooleanLiteral,
								"value": false,
							},
							"right": map[string]any{
								"type": parser.NullLiteral,
							},
						},
					},
				},
			},
		},
		{
			name:    "Logical Expression",
			program: `false || true && false;`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type": parser.LogicalExpression,
							"left": map[string]any{
								"type":  parser.BooleanLiteral,
								"value": false,
							},
							"operator": "||",
							"right": map[string]any{
								"type": parser.LogicalExpression,
								"left": map[string]any{
									"type":  parser.BooleanLiteral,
									"value": true,
								},
								"operator": "&&",
								"right": map[string]any{
									"type":  parser.BooleanLiteral,
									"value": false,
								},
							},
						},
					},
				},
			},
		},
		{
			name:    "Unary Expression",
			program: `!(-x > -y);`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type":     parser.UnaryExpression,
							"operator": "!",
							"argument": map[string]any{
								"type": parser.BinaryExpression,
								"left": map[string]any{
									"type":     parser.UnaryExpression,
									"operator": "-",
									"argument": map[string]any{
										"type": parser.Identifier,
										"name": "x",
									},
								},
								"operator": ">",
								"right": map[string]any{
									"type":     parser.UnaryExpression,
									"operator": "-",
									"argument": map[string]any{
										"type": parser.Identifier,
										"name": "y",
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "While Statement",
			program: `
				while (foo) {
					foo = false;
				}
			`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.WhileStatement,
						"test": map[string]any{
							"type": parser.Identifier,
							"name": "foo",
						},
						"body": map[string]any{
							"type": parser.BlockStatement,
							"body": []map[string]any{
								{
									"type": parser.ExpressionStatement,
									"expression": map[string]any{
										"type":     parser.AssignmentExpression,
										"operator": "=",
										"left": map[string]any{
											"type": parser.Identifier,
											"name": "foo",
										},
										"right": map[string]any{
											"type":  parser.BooleanLiteral,
											"value": false,
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "Do-While Statement",
			program: `
				do {
					foo = false;
				} while (foo)
			`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.DoWhileStatement,
						"test": map[string]any{
							"type": parser.Identifier,
							"name": "foo",
						},
						"body": map[string]any{
							"type": parser.BlockStatement,
							"body": []map[string]any{
								{
									"type": parser.ExpressionStatement,
									"expression": map[string]any{
										"type":     parser.AssignmentExpression,
										"operator": "=",
										"left": map[string]any{
											"type": parser.Identifier,
											"name": "foo",
										},
										"right": map[string]any{
											"type":  parser.BooleanLiteral,
											"value": false,
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "For Statement",
			program: `
				for (let i=0; i<10; i+=1) {
					27;
				}
			`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ForStatement,
						"init": map[string]any{
							"type": parser.VariableDeclarationStatement,
							"declarations": []map[string]any{
								{
									"type": parser.VariableDeclarator,
									"id": map[string]any{
										"type": parser.Identifier,
										"name": "i",
									},
									"init": map[string]any{
										"type":  parser.NumericLiteral,
										"value": int64(0),
									},
								},
							},
						},
						"test": map[string]any{
							"type": parser.BinaryExpression,
							"left": map[string]any{
								"type": parser.Identifier,
								"name": "i",
							},
							"operator": "\u003c",
							"right": map[string]any{
								"type":  parser.NumericLiteral,
								"value": int64(10),
							},
						},
						"update": map[string]any{
							"type": parser.AssignmentExpression,
							"left": map[string]any{
								"type": parser.Identifier,
								"name": "i",
							},
							"operator": "+=",
							"right": map[string]any{
								"type":  parser.NumericLiteral,
								"value": int64(1),
							},
						},
						"body": map[string]any{
							"type": parser.BlockStatement,
							"body": []map[string]any{
								{
									"type": parser.ExpressionStatement,
									"expression": map[string]any{
										"type":  parser.NumericLiteral,
										"value": int64(27),
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "Function Declaration",
			program: `
				function me(x) {
				  return x;  
				}
			`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.FunctionDeclaration,
						"id": map[string]any{
							"type": parser.Identifier,
							"name": "me",
						},
						"params": []map[string]any{
							{
								"type": parser.Identifier,
								"name": "x",
							},
						},
						"body": map[string]any{
							"type": parser.BlockStatement,
							"body": []map[string]any{
								{
									"type": parser.ReturnStatement,
									"argument": map[string]any{
										"type": parser.Identifier,
										"name": "x",
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "Function Call",
			program: `
				me(x,1+2);
			`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type": parser.CallExpression,
							"callee": map[string]any{
								"type": parser.Identifier,
								"name": "me",
							},
							"arguments": []map[string]any{
								{
									"type": parser.Identifier,
									"name": "x",
								},
								{
									"type":     parser.BinaryExpression,
									"operator": "+",
									"left": map[string]any{
										"type":  parser.NumericLiteral,
										"value": int64(1),
									},
									"right": map[string]any{
										"type":  parser.NumericLiteral,
										"value": int64(2),
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "Chained Function Call",
			program: `
				f()();
			`,
			ast: map[string]any{
				"type": "Program",
				"body": []map[string]any{
					{
						"type": parser.ExpressionStatement,
						"expression": map[string]any{
							"type": parser.CallExpression,
							"callee": map[string]any{
								"type": parser.CallExpression,
								"callee": map[string]any{
									"type": parser.Identifier,
									"name": "f",
								},
								"arguments": []map[string]any(nil),
							},
							"arguments": []map[string]any(nil),
						},
					},
				},
			},
		},
	}

	for _, tc := range tt {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			pr := parser.NewParser(tc.program)
			ast, err := pr.Parse()
			require.NoError(t, err)
			assert.Equal(t, tc.ast, ast)
		})
	}
}
