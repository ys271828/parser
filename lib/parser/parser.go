package parser

import (
	"errors"
	"fmt"
	"strconv"
)

const (
	AssignmentExpression         = "AssignmentExpression"
	BinaryExpression             = "BinaryExpression"
	BlockStatement               = "BlockStatement"
	BooleanLiteral               = "BooleanLiteral"
	CallExpression               = "CallExpression"
	DoWhileStatement             = "DoWhileStatement"
	EmptyStatement               = "EmptyStatement"
	ExpressionStatement          = "ExpressionStatement"
	ForStatement                 = "ForStatement"
	FunctionDeclaration          = "FunctionDeclaration"
	Identifier                   = "Identifier"
	IfStatement                  = "IfStatement"
	LogicalExpression            = "LogicalExpression"
	NullLiteral                  = "NullLiteral"
	NumericLiteral               = "NumericLiteral"
	Program                      = "Program"
	ReturnStatement              = "ReturnStatement"
	StringLiteral                = "StringLiteral"
	UnaryExpression              = "UnaryExpression"
	VariableDeclarator           = "VariableDeclarator"
	VariableDeclarationStatement = "VariableDeclarationStatement"
	WhileStatement               = "WhileStatement"
)

var ErrSyntax = errors.New("syntax error")

type Parser struct {
	str       string
	tokenizer *Tokenizer
	lookahead *Token
}

func NewParser(str string) *Parser {
	return &Parser{
		str:       str,
		tokenizer: NewTokenizer(str),
	}
}

func (p *Parser) Parse() (map[string]any, error) {
	lookahead, err := p.tokenizer.getNextToken()
	if err != nil {
		return nil, err
	}
	p.lookahead = lookahead
	return p.program()
}

func (p *Parser) program() (map[string]any, error) {
	body, err := p.statementList(nil)
	if err != nil {
		return nil, err
	}
	return map[string]any{
		"type": Program,
		"body": body,
	}, nil
}

func (p *Parser) pop(tt TokenType) (*Token, error) {
	token := p.lookahead

	if token == nil {
		return nil, fmt.Errorf("%w: unexpected end of input, expected [%v]", ErrSyntax, tt)
	}

	if token.Type != tt {
		return nil, fmt.Errorf("%w: unexpected token [%v], expected [%v]", ErrSyntax, token.Type, tt)
	}

	lookahead, err := p.tokenizer.getNextToken()
	if err != nil {
		return nil, err
	}
	p.lookahead = lookahead

	return token, nil
}

func (p *Parser) statementList(stopLookahead *TokenType) ([]map[string]any, error) {
	st, err := p.statement()
	if err != nil {
		return nil, err
	}
	statementList := []map[string]any{st}

	for p.lookahead != nil && (stopLookahead == nil || p.lookahead.Type != *stopLookahead) {
		st, err := p.statement()
		if err != nil {
			return nil, err
		}
		statementList = append(statementList, st)
	}

	return statementList, nil
}

func (p *Parser) statement() (map[string]any, error) {
	if p.lookahead == nil {
		return nil, nil
	}
	switch p.lookahead.Type {
	case TokenTypeSemiColon:
		return p.emptyStatement()
	case TokenTypeIf:
		return p.ifStatement()
	case TokenTypeOpenBrace:
		return p.blockStatement()
	case TokenTypeLet:
		return p.variableDeclarationStatement()
	case TokenTypeFunction:
		return p.functionDeclaration()
	case TokenTypeReturn:
		return p.returnStatement()
	case TokenTypeWhile:
		return p.whileStatement()
	case TokenTypeDo:
		return p.doWhileStatement()
	case TokenTypeFor:
		return p.forStatement()
	default:
		return p.expressionStatement()
	}
}

func (p *Parser) returnStatement() (map[string]any, error) {
	if _, err := p.pop(TokenTypeReturn); err != nil {
		return nil, err
	}
	var argument map[string]any
	if p.lookahead != nil && p.lookahead.Type != TokenTypeSemiColon {
		var err error
		if argument, err = p.expression(); err != nil {
			return nil, err
		}
	}
	if _, err := p.pop(TokenTypeSemiColon); err != nil {
		return nil, err
	}
	return map[string]any{
		"type":     ReturnStatement,
		"argument": argument,
	}, nil
}

func (p *Parser) functionDeclaration() (map[string]any, error) {
	if _, err := p.pop(TokenTypeFunction); err != nil {
		return nil, err
	}

	id, err := p.identifier()
	if err != nil {
		return nil, err
	}

	if _, err := p.pop(TokenTypeOpenParenthesis); err != nil {
		return nil, err
	}

	var params []map[string]any
	if p.lookahead != nil && p.lookahead.Type != TokenTypeCloseParenthesis {
		params, err = p.functionParameterList()
		if err != nil {
			return nil, err
		}
	}

	if _, err := p.pop(TokenTypeCloseParenthesis); err != nil {
		return nil, err
	}

	body, err := p.blockStatement()
	if err != nil {
		return nil, err
	}

	return map[string]any{
		"type":   FunctionDeclaration,
		"id":     id,
		"params": params,
		"body":   body,
	}, nil
}

func (p *Parser) functionParameterList() ([]map[string]any, error) {
	var params []map[string]any

	done := false
	for !done {
		identifier, err := p.identifier()
		if err != nil {
			return nil, err
		}
		params = append(params, identifier)
		if p.lookahead != nil && p.lookahead.Type == TokenTypeComma {
			_, _ = p.pop(TokenTypeComma)
		} else {
			done = true
		}
	}

	return params, nil
}

func (p *Parser) whileStatement() (map[string]any, error) {
	if _, err := p.pop(TokenTypeWhile); err != nil {
		return nil, err
	}

	if _, err := p.pop(TokenTypeOpenParenthesis); err != nil {
		return nil, err
	}

	test, err := p.expression()
	if err != nil {
		return nil, err
	}

	if _, err := p.pop(TokenTypeCloseParenthesis); err != nil {
		return nil, err
	}

	body, err := p.statement()
	if err != nil {
		return nil, err
	}

	return map[string]any{
		"type": WhileStatement,
		"test": test,
		"body": body,
	}, nil
}

func (p *Parser) doWhileStatement() (map[string]any, error) {
	if _, err := p.pop(TokenTypeDo); err != nil {
		return nil, err
	}

	body, err := p.statement()
	if err != nil {
		return nil, err
	}

	if _, err := p.pop(TokenTypeWhile); err != nil {
		return nil, err
	}

	if _, err := p.pop(TokenTypeOpenParenthesis); err != nil {
		return nil, err
	}

	test, err := p.expression()
	if err != nil {
		return nil, err
	}

	if _, err := p.pop(TokenTypeCloseParenthesis); err != nil {
		return nil, err
	}

	return map[string]any{
		"type": DoWhileStatement,
		"body": body,
		"test": test,
	}, nil
}

func (p *Parser) forStatement() (map[string]any, error) {
	if _, err := p.pop(TokenTypeFor); err != nil {
		return nil, err
	}

	if _, err := p.pop(TokenTypeOpenParenthesis); err != nil {
		return nil, err
	}

	var err error

	var init map[string]any
	if p.lookahead != nil && p.lookahead.Type != TokenTypeSemiColon {
		init, err = p.forStatementInit()
		if err != nil {
			return nil, err
		}
	}
	if _, err := p.pop(TokenTypeSemiColon); err != nil {
		return nil, err
	}

	var test map[string]any
	if p.lookahead != nil && p.lookahead.Type != TokenTypeSemiColon {
		test, err = p.expression()
		if err != nil {
			return nil, err
		}
	}
	if _, err := p.pop(TokenTypeSemiColon); err != nil {
		return nil, err
	}

	var update map[string]any
	if p.lookahead != nil && p.lookahead.Type != TokenTypeCloseParenthesis {
		update, err = p.expression()
		if err != nil {
			return nil, err
		}
	}
	if _, err := p.pop(TokenTypeCloseParenthesis); err != nil {
		return nil, err
	}

	body, err := p.statement()
	if err != nil {
		return nil, err
	}

	return map[string]any{
		"type":   ForStatement,
		"init":   init,
		"test":   test,
		"update": update,
		"body":   body,
	}, nil
}

func (p *Parser) forStatementInit() (map[string]any, error) {
	if p.lookahead != nil && p.lookahead.Type == TokenTypeLet {
		return p.variableDeclarationStatementInit()
	}
	return p.expression()
}

func (p *Parser) ifStatement() (map[string]any, error) {
	if _, err := p.pop(TokenTypeIf); err != nil {
		return nil, err
	}

	if _, err := p.pop(TokenTypeOpenParenthesis); err != nil {
		return nil, err
	}

	test, err := p.expression()
	if err != nil {
		return nil, err
	}

	if _, err := p.pop(TokenTypeCloseParenthesis); err != nil {
		return nil, err
	}

	consequent, err := p.statement()
	if err != nil {
		return nil, err
	}

	var alternate map[string]any
	if p.lookahead != nil && p.lookahead.Type == TokenTypeElse {
		if _, err := p.pop(TokenTypeElse); err != nil {
			return nil, err
		}
		if alternate, err = p.statement(); err != nil {
			return nil, err
		}
	}

	return map[string]any{
		"type":       IfStatement,
		"test":       test,
		"consequent": consequent,
		"alternate":  alternate,
	}, nil
}

func (p *Parser) variableDeclarationStatement() (map[string]any, error) {
	statement, err := p.variableDeclarationStatementInit()
	if err != nil {
		return nil, err
	}

	_, _ = p.pop(TokenTypeSemiColon)

	return statement, nil
}

func (p *Parser) variableDeclarationStatementInit() (map[string]any, error) {
	_, _ = p.pop(TokenTypeLet)

	declarations, err := p.variableDeclaratorList()
	if err != nil {
		return nil, err
	}

	return map[string]any{
		"type":         VariableDeclarationStatement,
		"declarations": declarations,
	}, nil
}

func (p *Parser) variableDeclaratorList() ([]map[string]any, error) {
	var declarations []map[string]any

	done := false
	for !done {
		decl, err := p.variableDeclarator()
		if err != nil {
			return nil, err
		}

		declarations = append(declarations, decl)

		if p.lookahead != nil && p.lookahead.Type == TokenTypeComma {
			_, _ = p.pop(TokenTypeComma)
		} else {
			done = true
		}
	}

	return declarations, nil
}

func (p *Parser) variableDeclarator() (map[string]any, error) {
	identifier, err := p.identifier()
	if err != nil {
		return nil, err
	}

	var init map[string]any
	if p.lookahead.Type != TokenTypeSemiColon && p.lookahead.Type != TokenTypeComma {
		if init, err = p.variableInitializer(); err != nil {
			return nil, err
		}
	}

	return map[string]any{
		"type": VariableDeclarator,
		"id":   identifier,
		"init": init,
	}, nil
}

func (p *Parser) variableInitializer() (map[string]any, error) {
	_, _ = p.pop(TokenTypeSimpleAssignment)
	return p.assignmentExpression()
}

func (p *Parser) emptyStatement() (map[string]any, error) {
	_, _ = p.pop(TokenTypeSemiColon)

	return map[string]any{
		"type": EmptyStatement,
	}, nil
}

func (p *Parser) blockStatement() (map[string]any, error) {
	_, _ = p.pop(TokenTypeOpenBrace)

	var body []map[string]any
	if p.lookahead.Type != TokenTypeCloseBrace {
		var err error
		if body, err = p.statementList(addr(TokenTypeCloseBrace)); err != nil {
			return nil, err
		}
	}

	_, _ = p.pop(TokenTypeCloseBrace)

	return map[string]any{
		"type": BlockStatement,
		"body": body,
	}, nil
}

func (p *Parser) expressionStatement() (map[string]any, error) {
	expression, err := p.expression()
	if err != nil {
		return nil, err
	}
	_, _ = p.pop(TokenTypeSemiColon)
	return map[string]any{
		"type":       ExpressionStatement,
		"expression": expression,
	}, nil
}

func (p *Parser) expression() (map[string]any, error) {
	return p.assignmentExpression()
}

func (p *Parser) assignmentExpression() (map[string]any, error) {
	left, err := p.logicalOrExpression()
	if err != nil {
		return nil, err
	}

	if p.lookahead == nil || !p.lookahead.Type.isAssignment() {
		return left, nil
	}

	var op *Token
	if p.lookahead.Type == TokenTypeSimpleAssignment {
		op, _ = p.pop(TokenTypeSimpleAssignment)
	} else {
		op, _ = p.pop(TokenTypeSelfOperation)
	}

	if t, _ := left["type"].(string); t != Identifier {
		return nil, fmt.Errorf("%w: invalid left-hand side for assignment expression", ErrSyntax)
	}

	right, err := p.assignmentExpression()
	if err != nil {
		return nil, err
	}

	return map[string]any{
		"type":     AssignmentExpression,
		"operator": op.Value,
		"left":     left,
		"right":    right,
	}, nil
}

func (p *Parser) unaryExpression() (map[string]any, error) {
	if p.lookahead == nil {
		return nil, nil
	}

	var op *Token
	var err error
	switch p.lookahead.Type {
	case TokenTypeAdditiveOperator:
		if op, err = p.pop(TokenTypeAdditiveOperator); err != nil {
			return nil, err
		}
	case TokenTypeLogicalNot:
		if op, err = p.pop(TokenTypeLogicalNot); err != nil {
			return nil, err
		}
	}

	if op != nil {
		argument, err := p.unaryExpression()
		if err != nil {
			return nil, err
		}
		return map[string]any{
			"type":     UnaryExpression,
			"operator": op.Value,
			"argument": argument,
		}, nil
	}

	return p.leftHandSideExpression()
}

func (p *Parser) leftHandSideExpression() (map[string]any, error) {
	expr, err := p.primaryExpression()
	if err != nil {
		return nil, err
	}

	if p.lookahead != nil && p.lookahead.Type == TokenTypeOpenParenthesis {
		return p.callExpression(expr)
	}

	return expr, nil
}

func (p *Parser) callExpression(callee map[string]any) (map[string]any, error) {
	if _, err := p.pop(TokenTypeOpenParenthesis); err != nil {
		return nil, err
	}

	var callArguments []map[string]any
	if p.lookahead != nil && p.lookahead.Type != TokenTypeCloseParenthesis {
		var err error
		if callArguments, err = p.callArguments(); err != nil {
			return nil, err
		}
	}

	if _, err := p.pop(TokenTypeCloseParenthesis); err != nil {
		return nil, err
	}

	callExpression := map[string]any{
		"type":      CallExpression,
		"callee":    callee,
		"arguments": callArguments,
	}

	if p.lookahead != nil && p.lookahead.Type == TokenTypeOpenParenthesis {
		var err error
		if callExpression, err = p.callExpression(callExpression); err != nil {
			return nil, err
		}
	}

	return callExpression, nil
}

func (p *Parser) callArguments() ([]map[string]any, error) {
	var arguments []map[string]any

	done := false
	for !done {
		arg, err := p.assignmentExpression()
		if err != nil {
			return nil, err
		}

		arguments = append(arguments, arg)

		if p.lookahead != nil && p.lookahead.Type == TokenTypeComma {
			_, _ = p.pop(TokenTypeComma)
		} else {
			done = true
		}
	}

	return arguments, nil
}

func (p *Parser) identifier() (map[string]any, error) {
	name, _ := p.pop(TokenTypeIdentifier)
	return map[string]any{
		"type": Identifier,
		"name": name.Value,
	}, nil
}

func (p *Parser) equalityExpression() (map[string]any, error) {
	return p.binaryExpression(p.comparisonExpression, TokenTypeEquality)
}

func (p *Parser) comparisonExpression() (map[string]any, error) {
	return p.binaryExpression(p.additiveExpression, TokenTypeComparisonOperator)
}

func (p *Parser) additiveExpression() (map[string]any, error) {
	return p.binaryExpression(p.multiplicativeExpression, TokenTypeAdditiveOperator)
}

func (p *Parser) multiplicativeExpression() (map[string]any, error) {
	return p.binaryExpression(p.unaryExpression, TokenTypeMultiplicativeOperator)
}

func (p *Parser) binaryExpression(higherExpressionFunc func() (map[string]any, error), tt TokenType) (map[string]any, error) {
	left, err := higherExpressionFunc()
	if err != nil {
		return nil, err
	}

	for p.lookahead != nil && p.lookahead.Type == tt {
		operator, _ := p.pop(tt)
		right, err := higherExpressionFunc()
		if err != nil {
			return nil, err
		}
		left = map[string]any{
			"type":     BinaryExpression,
			"operator": operator.Value,
			"left":     left,
			"right":    right,
		}
	}

	return left, nil
}

func (p *Parser) primaryExpression() (map[string]any, error) {
	if p.lookahead == nil {
		return nil, nil
	}
	if p.lookahead.Type.isLiteral() {
		return p.literal()
	}
	switch p.lookahead.Type {
	case TokenTypeOpenParenthesis:
		return p.parenthesizedExpression()
	case TokenTypeIdentifier:
		return p.identifier()
	default:
		return p.leftHandSideExpression()
	}
}

func (p *Parser) parenthesizedExpression() (map[string]any, error) {
	_, _ = p.pop(TokenTypeOpenParenthesis)
	expression, err := p.expression()
	if err != nil {
		return nil, err
	}
	_, _ = p.pop(TokenTypeCloseParenthesis)
	return expression, nil
}

func (p *Parser) literal() (map[string]any, error) {
	if p.lookahead == nil {
		return nil, nil
	}
	switch p.lookahead.Type {
	case TokenTypeNumber:
		return p.numericLiteral()
	case TokenTypeString:
		return p.stringLiteral()
	case TokenTypeTrue, TokenTypeFalse:
		return p.booleanLiteral(p.lookahead.Type)
	case TokenTypeNull:
		return p.nullLiteral()
	}
	err := fmt.Errorf("%w: unsupported token type [%v]", ErrSyntax, p.lookahead.Type)
	return nil, err
}

func (p *Parser) nullLiteral() (map[string]any, error) {
	if _, err := p.pop(TokenTypeNull); err != nil {
		return nil, err
	}

	return map[string]any{
		"type": NullLiteral,
	}, nil
}

func (p *Parser) booleanLiteral(tokenType TokenType) (map[string]any, error) {
	if _, err := p.pop(tokenType); err != nil {
		return nil, err
	}

	var value bool
	if tokenType == TokenTypeTrue {
		value = true
	}

	return map[string]any{
		"type":  BooleanLiteral,
		"value": value,
	}, nil
}

func (p *Parser) numericLiteral() (map[string]any, error) {
	token, err := p.pop(TokenTypeNumber)
	if err != nil {
		return nil, err
	}

	value, err := strconv.ParseInt(token.Value, 10, 64)
	if err != nil {
		return nil, err
	}

	return map[string]any{
		"type":  NumericLiteral,
		"value": value,
	}, nil
}

func (p *Parser) stringLiteral() (map[string]any, error) {
	token, err := p.pop(TokenTypeString)
	if err != nil {
		return nil, err
	}

	return map[string]any{
		"type":  StringLiteral,
		"value": token.Value[1 : len(token.Value)-1],
	}, nil
}

func (p *Parser) logicalAndExpression() (map[string]any, error) {
	return p.logicalExpression(p.equalityExpression, TokenTypeLogicalAnd)
}

func (p *Parser) logicalOrExpression() (map[string]any, error) {
	return p.logicalExpression(p.logicalAndExpression, TokenTypeLogicalOr)
}

func (p *Parser) logicalExpression(higherExpressionFunc func() (map[string]any, error), tt TokenType) (map[string]any, error) {
	left, err := higherExpressionFunc()
	if err != nil {
		return nil, err
	}

	for p.lookahead != nil && p.lookahead.Type == tt {
		operator, _ := p.pop(tt)
		right, err := higherExpressionFunc()
		if err != nil {
			return nil, err
		}
		left = map[string]any{
			"type":     LogicalExpression,
			"operator": operator.Value,
			"left":     left,
			"right":    right,
		}
	}

	return left, nil
}

func addr[T any](v T) *T {
	return &v
}
