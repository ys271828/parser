package parser

import (
	"fmt"
	"regexp"
)

type Tokenizer struct {
	str string
	idx int
}

func NewTokenizer(str string) *Tokenizer {
	return &Tokenizer{str: str}
}

func (t *Tokenizer) hasMoreTokens() bool {
	return t.idx < len(t.str)
}

type tokenRegexp struct {
	re        *regexp.Regexp
	tokenType TokenType
}

var tokenRegexps = []tokenRegexp{
	// whitespaces
	{regexp.MustCompile(`^\s+`), TokenTypeIgnore},
	// comments
	{regexp.MustCompile(`^//.*`), TokenTypeIgnore},
	{regexp.MustCompile(`^/\*[\s\S]*?\*/`), TokenTypeIgnore},
	// delimiters
	{regexp.MustCompile(`^;`), TokenTypeSemiColon},
	{regexp.MustCompile(`^\{`), TokenTypeOpenBrace},
	{regexp.MustCompile(`^}`), TokenTypeCloseBrace},
	{regexp.MustCompile(`^\(`), TokenTypeOpenParenthesis},
	{regexp.MustCompile(`^\)`), TokenTypeCloseParenthesis},
	{regexp.MustCompile(`^,`), TokenTypeComma},
	// keywords
	{regexp.MustCompile(`^\blet\b`), TokenTypeLet},
	{regexp.MustCompile(`^\bif\b`), TokenTypeIf},
	{regexp.MustCompile(`^\belse\b`), TokenTypeElse},
	{regexp.MustCompile(`^\btrue\b`), TokenTypeTrue},
	{regexp.MustCompile(`^\bfalse\b`), TokenTypeFalse},
	{regexp.MustCompile(`^\bnull\b`), TokenTypeNull},
	{regexp.MustCompile(`^\bwhile\b`), TokenTypeWhile},
	{regexp.MustCompile(`^\bdo\b`), TokenTypeDo},
	{regexp.MustCompile(`^\bfor\b`), TokenTypeFor},
	{regexp.MustCompile(`^\bfunction\b`), TokenTypeFunction},
	{regexp.MustCompile(`^\breturn\b`), TokenTypeReturn},
	// identifiers
	{regexp.MustCompile(`^[a-zA-Z_][0-9a-zA-Z_]*`), TokenTypeIdentifier},
	// equality
	{regexp.MustCompile(`^[!=]=`), TokenTypeEquality},
	// assignments
	{regexp.MustCompile(`^=`), TokenTypeSimpleAssignment},
	{regexp.MustCompile(`^[*/+\-]=`), TokenTypeSelfOperation},
	// maths
	{regexp.MustCompile(`^[+-]`), TokenTypeAdditiveOperator},
	{regexp.MustCompile(`^[*/]`), TokenTypeMultiplicativeOperator},
	// comparisons
	{regexp.MustCompile(`^[<>]=?`), TokenTypeComparisonOperator},
	// logical
	{regexp.MustCompile(`^&&`), TokenTypeLogicalAnd},
	{regexp.MustCompile(`^\|\|`), TokenTypeLogicalOr},
	{regexp.MustCompile(`^!`), TokenTypeLogicalNot},
	// number literals
	{regexp.MustCompile(`^\d+`), TokenTypeNumber},
	// string literals
	{regexp.MustCompile(`^"[^"]*"`), TokenTypeString},
	{regexp.MustCompile(`^'[^']*'`), TokenTypeString},
}

func (t *Tokenizer) getNextToken() (*Token, error) {
	if !t.hasMoreTokens() {
		return nil, nil
	}

	for _, tr := range tokenRegexps {
		re := tr.re
		tokenType := tr.tokenType
		if value := re.FindString(t.str[t.idx:]); value != "" {
			t.idx += len(value)
			if tokenType == TokenTypeIgnore {
				return t.getNextToken()
			}
			return &Token{Type: tokenType, Value: value}, nil
		}
	}

	return nil, fmt.Errorf("%w: unexpected token [%s]", ErrSyntax, string(t.str[t.idx]))
}
