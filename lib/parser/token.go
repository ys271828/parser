package parser

type TokenType int

const (
	TokenTypeInvalid TokenType = iota
	TokenTypeAdditiveOperator
	TokenTypeCloseBrace
	TokenTypeCloseParenthesis
	TokenTypeComma
	TokenTypeComparisonOperator
	TokenTypeDo
	TokenTypeElse
	TokenTypeEquality
	TokenTypeFalse
	TokenTypeFor
	TokenTypeFunction
	TokenTypeIdentifier
	TokenTypeIf
	TokenTypeIgnore
	TokenTypeLet
	TokenTypeLogicalAnd
	TokenTypeLogicalNot
	TokenTypeLogicalOr
	TokenTypeMultiplicativeOperator
	TokenTypeNull
	TokenTypeNumber
	TokenTypeOpenBrace
	TokenTypeOpenParenthesis
	TokenTypeReturn
	TokenTypeSelfOperation
	TokenTypeSemiColon
	TokenTypeSimpleAssignment
	TokenTypeString
	TokenTypeTrue
	TokenTypeWhile
)

type Token struct {
	Type  TokenType
	Value string
}

func (tt TokenType) isLiteral() bool {
	return tt == TokenTypeNumber || tt == TokenTypeString || tt == TokenTypeTrue || tt == TokenTypeFalse || tt == TokenTypeNull
}

func (tt TokenType) isAssignment() bool {
	return tt == TokenTypeSimpleAssignment || tt == TokenTypeSelfOperation
}
